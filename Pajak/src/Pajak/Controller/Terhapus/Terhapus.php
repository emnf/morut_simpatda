<?php

namespace Pajak\Controller\Terhapus;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Pajak\Model\Terhapus\TerhapusBase;

class Terhapus extends AbstractActionController {

    public function indexAction() {
        $req = $this->getRequest();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd0 = $this->Tools()->getService('PejabatTable')->getdata();
        $records = array();
        foreach ($ar_ttd0 as $ar_ttd0) {
            $records[] = $ar_ttd0;
        }
        $view = new ViewModel(array(
            'ar_ttd0' => $records
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $post = $req->getPost();
        $base = new \Pajak\Model\Terhapus\TerhapusBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('TerhapusTable')->getGridCount($base, $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('TerhapusTable')->getGridData($base, $start, $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        foreach ($data as $row) {
          if ($row['t_userhapus'] == '99'){
              $a = 'SISTEM';
          }else{
              $a = $row['s_nama'];
          }
              
            $s .= "<tr>";
            $s .= "<td><center>" . $counter . "</center></td>";
            $s .= "<td><center>" . date('d-m-Y', strtotime($row['t_tglpendataan'])) . "</center></td>";
            $s .= "<td><center>" . $row['t_npwpdwp'] . "</center></td>";
            $s .= "<td>" . $row['t_namawp'] . "</td>";
            $s .= "<td>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td><center>" . $row['t_nourut'] . "</center></td>";
            $s .= "<td>" . $row['s_namajenis'] . "</td>";
            $s .= "<td><center>" . date('d-m-Y', strtotime($row['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($row['t_masaakhir'])) . "  </center></td>";
            $s .= "<td style='text-align:right'>" . number_format($row['t_jmlhpajak'], 0, ',', '.') . "</td>";
            $s .= "<td><center> $a</center></td>";
           
           
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

   

    public function cetakrekapterhapusAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data = $this->Tools()->getService('TerhapusTable')->getdataterhapus($data_get->tglpendataan0,$data_get->tglpendataan1);
        // var_dump($data_get->tahunper31);exit;
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $ar_ttd0 = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get->ttd0);
        // var_dump($data_get->jnscetak);exit;
        
        if ($data_get->jnscetak == 1) {
            $pdf = new \LosPdf\View\Model\PdfModel();
            $pdf->setVariables(array(
                'data' => $data,
                'ar_pemda' => $ar_pemda,
                'tglpendataan0' => $data_get->tglpendataan0,
                'tglpendataan1' => $data_get->tglpendataan1,
                'ttd0' => $ar_ttd0,
                'jnscetak'=>$data_get->jnscetak 
            ));
            $pdf->setOption("paperSize", "legal-L");
            return $pdf;
        }else if ($data_get->jnscetak == 2) {
          
            $view = new ViewModel(array(
                'data' => $data,
                'ar_pemda' => $ar_pemda,
                'tglpendataan0' => $data_get->tglpendataan0,
                'tglpendataan1' => $data_get->tglpendataan1,
                'ttd0' => $ar_ttd0,
                'jnscetak'=>$data_get->jnscetak 
               
            ));
            $data = array(
                'nilai' => '3' //no layout
            );
            $this->layout()->setVariables($data);
            return $view;
        }
    }

}
