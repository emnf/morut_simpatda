<?php

namespace Pajak\Controller\Fiskal;

use Zend\Debug\Debug;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class Fiskal extends AbstractActionController
{

    public function indexAction()
    {
        $req = $this->getRequest();
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }

        $ar_pejabat = $this->Tools()->getService('PejabatTable')->getdata();
        $recordspejabat = array();
        foreach ($ar_pejabat as $ar_pejabat) {
            $recordspejabat[] = $ar_pejabat;
        }
        $view = new ViewModel(array(
            'dataobjek' => $recordspajak,
            'ar_pejabat' => $recordspejabat
        ));
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $post = $req->getPost();
        $base = new \Pajak\Model\Fiskal\FiskalBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('FiskalTable')->getGridCount($base, $session, $post);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('FiskalTable')->getGridData($base, $start, $session, $post);
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        if ($count == 0) {
            $s .= "<tr><td colspan='10' style='padding:30px 0px 30px 0px;'><p class='no-data-image'></p>"
                . "<h3 class='text-center'>Belum Ada Data Tersedia</h3>"
                . "<h5 class='text-center'>Mohon maaf, sepertinya belum ada data yang dapat kami tampilkan</h5></td></tr>";
        }
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td data-title='No'><center>" . $counter . "</center></td>";
            $s .= "<td data-title='No. Urut'><center><a href='javascript:void(0)' class='btn btn-warning btn-xs' style='font-size:10pt;'><b>" . str_pad($row['t_nomorurut'], 4, '0', STR_PAD_LEFT) . "</b></a></center></td>";
            $s .= "<td data-title='Tanggal Daftar'><center>" . date('d-m-Y', strtotime($row['t_tgldaftar'])) . "</center></td>";
            $s .= "<td data-title='NPWPD'><center><b style='color:red;'>" . $row['t_npwpd'] . "</b></center></td>";
            $s .= "<td data-title='Nama WP'>" . $row['t_namaobjek'] . "</td>";
            $s .= "<td data-title='Nama Pemilik'><center>" . $row['t_pemilik'] . "</center></td>";
            $s .= "<td data-title='Alamat'>" . $row['t_alamat_lengkap'] . "</td>";
            $s .= "<td data-title='Jenis Usaha'><center>" . $row['s_namausaha'] . "</center></td>";
            $s .= "<td data-title='Tgl. Jatuh Tempo' style='text-align: center'>" . date('d-m-Y', strtotime($row['t_tgljatuhtempo'])) . "</td>";
            $s .= "<td data-title='#'><center><a href='fiskal/form_edit?t_idfiskal=$row[t_idfiskal]' class='btn btn-warning btn-xs btn-flat'><i class='glyph-icon icon-pencil'></i> Edit</a>";
            $s .= "<button onclick='bukaCetakFISKAL($row[t_idfiskal])' target='_blank' class='btn btn-primary btn-xs' title='Cetak SK FISKAL'><i class='glyph-icon icon-print'></i> Cetak SK FISKAL</button></center></td>";
            //            if (empty($row['t_tglpembayaran'])) {
            //            $s .= "<td data-title='Status Bayar'><center><button class='btn btn-xs btn-round btn-danger' title='Belum Bayar'><i class='glyph-icon icon-close'></i></button></center></td>";
            //            } else {
            //                $s .= "<td data-title='Status Bayar'>onclick='bukaDetailPenetapan(" . $row['t_idfiskal'] . ")' <center><button class='btn btn-xs btn-round btn-primary' title='Sudah Bayar'><i class='glyph-icon icon-check'></i></button></center></td>";
            //            }
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function dataGridWpAction()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $req = $this->getRequest();
        $req->isGet();
        $parametercari = $req->getQuery();
        $base = new \Pajak\Model\Pendaftaran\PendaftaranBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('PendaftaranTable')->getGridCountWpFiskal($base, $parametercari);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $s = "";
        $data = $this->Tools()->getService('PendaftaranTable')->getGridDataWpFiskal($base, $start, $parametercari);

        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td><center>" . $row['t_npwpd'] . "</center></td>";
            $s .= "<td>" . $row['t_nama'] . "</td>";
            $s .= "<td>" . $row['t_alamat'] . "</td>";
            $s .= "<td><a href='#' onclick='pilihWp(" . $row['t_idwp'] . ");return false;' class='btn btn-xs btn-primary'><span class='glyph-icon icon-hand-o-up'> Pilih </a></td>";
            $s .= "</tr>";
        }
        $data_render = array(
            "gridfiskal" => $s,
            "rowsfiskal" => 10,
            "countfiskal" => $count,
            "pagefiskal" => $page,
            "startfiskal" => $start,
            "total_halamanfiskal" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function pilihWpAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $dataWp = $this->Tools()->getService('FiskalTable')->getDataWpId($data_get['t_idwp']);
        $opsi = "";
        $dataObjek = $this->Tools()->getService('FiskalTable')->getDataObjekByIdWp($dataWp['t_idwp']);
        $opsi .= "<option value=''>Silahkan Pilih</option>";
        $no = 1;
        foreach ($dataObjek as $row) {
            $opsi .= "<option value='" . $row['t_idobjek'] . "'>" . $no++ . " || " . $row['t_namaobjek'] . " ( " . $row['s_namajenis'] . " )</option>";
        }
        $data = array(
            't_idwp' => $dataWp['t_idwp'],
            't_npwpd' => $dataWp['t_npwpd'],
            't_nama' => $dataWp['t_nama'],
            't_wpobjek' => $opsi,
        );
        if ($dataWp['t_kecamatan'] == 11) {
            $data['t_alamat_tinggal'] = strtoupper($dataWp['t_alamat'] . ", RT. " . $dataWp['t_rt'] . ", RW. " . $dataWp['t_rw'] . ", KEC. " . $dataWp['t_kecamatanluar'] . ", KEL. " . $dataWp['t_kelurahanluar'] . ", KAB. " . $dataWp['t_kabupaten']);
        } else {
            $data['t_alamat_tinggal'] = strtoupper($dataWp['t_alamat_lengkap']);
        }

        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function FormTambahAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $no = $this->Tools()->getService('FiskalTable')->nourutfiskal();
        $t_nomorurut = (int) $no['t_nomorurut'] + 1;
        $comboid_jenisusaha = $this->Tools()->getService('FiskalTable')->getcomboIdJenisusaha();
        $req = $this->getRequest();
        $form = new \Pajak\Form\Fiskal\FiskalFrm($t_nomorurut, $comboid_jenisusaha);
        //        if ($this->getRequest()->isGet()) {
        //            $datarekambank = $this->Tools()->getService('RekambankTable')->getRekambankId($req->getQuery()->t_idsbh);
        //            if ($req->getQuery()->t_idsbh != NULL):
        //                $form->bind($datarekambank);
        //                $form->get('t_tglsbh')->setValue(date('d-m-Y',strtotime($datarekambank->t_tglsbh)));
        //            endif;
        //            
        //        }
        if ($this->getRequest()->isPost()) {
            $base = new \Pajak\Model\Fiskal\FiskalBase();
            $form->setInputFilter($base->getInputFilter());
            $post = $req->getPost()->toArray();
            $form->setData($post);
            if ($form->isValid()) {
                $base->exchangeArray($form->getData());

                $data = $this->Tools()->getService('FiskalTable')->simpanfiskal($base, $session, $post);
                return $this->redirect()->toRoute('fiskal');
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            //            'dataobjek' => $recordspajak,
            //            't_nosbh' => $nosbh
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function FormEditAction()
    {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idfiskal');
            $data = $this->Tools()->getService('FiskalTable')->getFiskalId($id);
            $data_wp = $this->Tools()->getService('FiskalTable')->getDataFiskalWP($id);
            //            $data->t_nopendaftaran = str_pad((int) $data->t_nopendaftaran, 7, '0', STR_PAD_LEFT);
            $data->t_tgldaftar = date('d-m-Y', strtotime($data->t_tgldaftar));
            $data->t_tgldaftar_sebelumnya = date('d-m-Y', strtotime($data->t_tgldaftar));
            $data->t_npwpd = $data_wp['t_npwpd'];
            $data->t_nama = $data_wp['t_namaobjek'];
            $data->t_alamat = $data_wp['t_alamat_lengkap'];
            $data->t_alamat_tinggal = $data_wp['t_alamat_tinggal'];
            $data->t_tgljatuhtempo = date('d-m-Y', strtotime($data_wp['t_tgljatuhtempo']));
            $data->t_tgljatuhtempo_sebelumnya = date('d-m-Y', strtotime($data_wp['t_tgljatuhtempo']));
            $comboid_jenisusaha = $this->Tools()->getService('FiskalTable')->getcomboIdJenisusaha();
            $form = new \Pajak\Form\Fiskal\FiskalFrm(null, $comboid_jenisusaha);
            $form->bind($data);
        }
        $view = new ViewModel(array(
            'form' => $form,
        ));
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function carinpwpdAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data_wpobjek = $this->Tools()->getService('FiskalTable')->getDataWpObjek($data_get->t_wpobjek);
        $data = array(
            "t_idwpwr" => $data_wpobjek['t_idwp'],
            "t_idwpwrobjek" => $data_wpobjek['t_idobjek'],
            "t_nama_usaha" => $data_wpobjek['t_namaobjek'],
            "t_alamat_usaha" => $data_wpobjek['t_alamatlengkapobjek'],
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function dataFiskalAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data_wp = $this->Tools()->getService('FiskalTable')->getDataFiskalWP($data_get->idfiskal);
        $data = array(
            "idfiskal" => $data_wp['t_idfiskal']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data));
    }

    public function cetakdatafiskalAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $data = $this->Tools()->getService('FiskalTable')->getDataFiskalWP($data_get->idfiskal);

        if ($data['t_kecamatan'] == 11) {
            $data['t_alamat_tinggal'] = strtoupper($data['t_alamat'] . ", RT. " . $data['t_rt'] . ", RW. " . $data['t_rw'] . ", KEC. " . $data['t_kecamatanluar'] . ", KEL. " . $data['t_kelurahanluar'] . ", KAB. " . $data['t_kabupaten']);
        }
        $datapej = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get->t_mengetahui);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $data,
            //            'tglcetak' => $data_get->tglcetak,
            'ar_pemda' => $ar_pemda,
            'ar_pejabat' => $datapej
        ));
        $pdf->setOption("paperSize", "F4");
        return $pdf;
    }

    public function dataPenetapanAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $data = $this->Tools()->getService('PendataanTable')->getDataPendataanIDTrans($data_get['idtransaksi']);
        $html = "   <div class='row' style='padding-bottom:13px'>
                        &nbsp;&nbsp;&nbsp;<span style='text-align:right; background:black; color: orange; padding: 10px 10px; font-size: 16px; font-weight:bolder'>" . $data['t_npwpd'] . "</span>
                    </div>";
        $html .= "  <div class='content-box border-top border-green' style='font-size:11px'>
                        <h3 class='content-box-header clearfix' style='font-size:12px; color:blue; font-weight: bold;'>
                            Data WP
                        </h3>
                        <div class='content-box-wrapper'>
                            <div class='row'>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>Nama</div>
                                    <div class='col-sm-4'>
                                        : " . $data['t_nama'] . "
                                    </div>
                                </div>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>Alamat</div>
                                    <div class='col-sm-10'>
                                        : " . $data['t_alamat'] . "
                                        ,RT. " . $data['t_rt'] . "
                                        ,RW. " . $data['t_rw'] . "
                                        ,Desa/Kel. " . $data['s_namakel'] . "
                                        ,Kec. " . $data['s_namakec'] . "
                                        ,Kab. " . $data['t_kabupaten'] . "
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='content-box border-top border-green' style='font-size:11px'>
                        <h3 class='content-box-header clearfix' style='font-size:12px; color:blue; font-weight: bold;'>
                            Data OP
                        </h3>
                        <div class='content-box-wrapper'>
                            <div class='row'>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>NIOP</div>
                                    <div class='col-sm-4'>
                                        : " . $data['t_nop'] . "
                                    </div>
                                </div>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>Nama</div>
                                    <div class='col-sm-4'>
                                        : " . $data['t_namaobjek'] . "
                                    </div>
                                </div>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>Alamat</div>
                                    <div class='col-sm-10'>
                                        : " . $data['t_alamatobjek'] . "
                                        ,RT. " . $data['t_rtobjek'] . "
                                        ,RW. " . $data['t_rwobjek'] . "
                                        ,Desa/Kel. " . $data['s_namakelobjek'] . "
                                        ,Kec. " . $data['s_namakecobjek'] . "
                                        ,Kab. " . $data['t_kabupatenobjek'] . "
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='content-box border-top border-green' style='font-size:11px'>
                        <h3 class='content-box-header clearfix' style='font-size:12px; color:blue; font-weight: bold;'>
                            Data SKPD
                        </h3>
                        <div class='content-box-wrapper'>
                            <div class='row'>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>Tgl. Penetapan</div>
                                    <div class='col-sm-4'>
                                        : " . date('d-m-Y', strtotime($data['t_tglpenetapan'])) . "
                                    </div>
                                </div>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>Masa Pajak</div>
                                    <div class='col-sm-4'>
                                        : " . date('d-m-Y', strtotime($data['t_masaawal'])) . " s/d " . date('d-m-Y', strtotime($data['t_masaakhir'])) . "
                                    </div>
                                </div>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>Pajak</div>
                                    <div class='col-sm-10'>
                                        : Rp. " . number_format($data['t_jmlhpajak'], 0, ',', '.') . ",-
                                    </div>
                                </div>
                                <div class='col-sm-12'>
                                    <div class='col-sm-2'>Rekening</div>
                                    <div class='col-sm-10'>
                                        : " . $data['s_namakorek'] . "
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>";
        $data_render = array(
            "datapenetapan" => $html
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function cetakdaftarfiskalAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $ar_data = $this->Tools()->getService('FiskalTable')->getDataDaftarFiskal($data_get->tgldaftar0, $data_get->tgldaftar1, $data_get->jenisfiskal);
        // var_dump($data); exit();
        $datapej = $this->Tools()->getService('PejabatTable')->getPejabatId($data_get->t_mengetahui);
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'data' => $ar_data,
            'tgldaftar' => $data_get->tgldaftar0 . ' s/d ' . $data_get->tgldaftar1,
            'ar_pemda' => $ar_pemda,
            'ar_diperiksa' => $datapej
        ));
        $pdf->setOption("paperSize", "legal-L");
        return $pdf;
    }
}
