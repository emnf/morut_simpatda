<?php

namespace Pajak\Controller\Setting;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SettingSkpd extends AbstractActionController {
    
    public function cekurl()
     {
        $basePath = $this->getRequest()->getBasePath();
            $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
            $uri->setPath($basePath);
            $uri->setQuery(array());
            $uri->setFragment('');
            
        return $uri->getScheme() . '://' . $uri->getHost() . ':'.$_SERVER['SERVER_PORT'].'' . $uri->getPath();
    
     }
    
    public function indexAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $form = new \Pajak\Form\Setting\SkpdFrm();
        $view = new ViewModel(array('form' => $form));
        
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            
            'settingskpdaktif' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    // public function dataGridAction() {
    //     $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
    //     $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        
    //     $input = $this->getRequest();
    //     $aColumns = array('s_skpd.s_idskpd','s_skpd.s_namaskpd','s_skpd.jalan_skpd','s_kecamatan.s_namakec','s_kelurahan.s_namakel');
        
    //     $rResult = $this->Tools()->getService('SkpdTable')->semuadata_skpd($input, $aColumns, $session, $this->cekurl(), $allParams);    
        
    //     return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    // }

    public function dataGridAction() {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new \Pajak\Model\Setting\SkpdBase();
        $base->exchangeArray($allParams);
        if ($base->direction != 'undefined') {
            $base->page = $base->direction;
        }
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->Tools()->getService('SkpdTable')->getGridCountSkpd($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->Tools()->getService('SkpdTable')->getGridDataSkpd($base, $start);
        
        $s = "";
        $counter = 1 + ($base->page * $base->rows) - $base->rows;
        if ($counter <= 1) {
            $counter = 1;
        }
        // var_dump($data->current());exit();
        foreach ($data as $row) {
 
            $s .= "<tr>";
            $s .= "<td><center>" . $counter . "</center></td>";
            $s .= "<td>" . $row['t_namaskpd'] . "</td>";
            $s .= "<td>" . $row['t_jalanskpd'] . "</td>";
            $s .= "<td>" . $row['s_namakec'] . "</td>";
            $s .= "<td>" . $row['s_namakel'] . "</td>";
            $s .= "<td><center><a href='setting_skpd/edit?t_idskpd=$row[t_idskpd]' class='btn btn-warning btn-xs btn-flat'><i class='glyphicon glyphicon-pencil'></i> Edit</a> <a href='#' onclick='hapus(" . $row['t_idskpd'] . ");return false;' class='btn btn-danger btn-xs btn-flat'><i class='glyphicon glyphicon-trash'></i> Hapus</a></center></td>";
            $s .= "</tr>";
            $counter++;
        }
        $datapaginge = new \Pajak\Controller\Plugin\Paginator();
        $datapaging = $datapaginge->paginator($base->rows, $count, $page, $total_pages, $start);
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages,
            "paginatore" => $datapaging['paginatore'],
            "akhirdata" => $datapaging['akhirdata'],
            "awaldata" => $datapaging['awaldata']
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }


    
    public function tambahAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $datakecamatan = $this->Tools()->getService('SkpdTable')->getcomboIdKecamatan();
        $frm = new \Pajak\Form\Setting\SkpdFrm($datakecamatan);
        $req = $this->getRequest();

        if ($req->isPost()) {
            $kb = new \Pajak\Model\Setting\SkpdBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if ($frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                $this->Tools()->getService('SkpdTable')->savedata($kb, $session);
                return $this->redirect()->toRoute('setting_skpd');
            }
        }
        $view = new ViewModel(array("frm" => $frm));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'settingskpdaktif' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function editAction() {
        $session = $this->getServiceLocator()->get('PajakService')->getStorage()->read();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idskpd');
            $data = $this->Tools()->getService('SkpdTable')->getSkpdId($id);
            $data2 = new \Zend\Stdlib\ArrayObject;  
            $data2['t_idskpd'] = $data['t_idskpd'];
            $data2['t_namaskpd'] = $data['t_namaskpd'];
            $data2['t_jalanskpd'] = $data['t_jalanskpd'];
            $data2['t_idkecskpd'] = $data['t_idkecskpd'];
            $data2['t_idkelskpd'] = $data['t_idkelskpd'];
            
            $datakecamatan = $this->Tools()->getService('SkpdTable')->getcomboIdKecamatan();
            $datakelurahan = $this->comboKelurahanCamat($data2['t_idkecskpd']);
            $frm = new \Pajak\Form\Setting\SkpdFrm($datakecamatan, $datakelurahan);
            
            
            $frm->bind($data2);
        }
        $view = new ViewModel(array(
            'frm' => $frm
        ));
        $dataobjek = $this->Tools()->getService('RekeningTable')->getdataJenisObjek();
        $recordspajak = array();
        foreach ($dataobjek as $dataobjek) {
            $recordspajak[] = $dataobjek;
        }
        $data = array(
            'data_pemda' => $ar_pemda,
            'datauser' => $session,
            'dataobjek' => $recordspajak,
            'settingskpdaktif' => 1
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function comboKelurahanCamat($id_kecamatan) {
        $selectData = array();
        $data = $this->Tools()->getService('SkpdTable')->getByKecamatan($id_kecamatan);
        foreach ($data as $row) {
            $selectData[$row['s_idkel']] = str_pad($row['s_kodekel'], 3, "0", STR_PAD_LEFT) . " || " . $row['s_namakel'];
        }
        return $selectData;
    }
    
    public function comboKelurahanCamatAction() {
        
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            
            $data_get = $req->getPost();
            
                
                $cekid_kecamatan = $this->Tools()->getService('SkpdTable')->cekid_kecamatan($data_get->s_idkec);
                $s = "";
                if ($cekid_kecamatan['s_kodekec'] == '00') {
                    if(!empty($req->getPost('t_kecamatan_badan'))){
                        $s .= " <div class='col-sm-12'>
                                <label class='col-sm-2 '>Kecamatan</label>
                                <div class='col-md-4'>
                                    <input type='text' class='form-control' name='t_kecamatanluar_badan' id='t_kecamatanluar_badan'>
                                </div>
                                <label class='col-sm-1'>Kelurahan</label>
                                <div class='col-md-4'>
                                    <input type='text' class='form-control' name='t_kelurahanluar_badan' id='t_kelurahanluar_badan'>
                                </div>
                            </div>";
                    }else{
                    $s .= " <div class='col-sm-12'>
                                <label class='col-sm-2 '>Kecamatan</label>
                                <div class='col-md-4'>
                                    <input type='text' class='form-control' name='t_kecamatanluar' id='t_kecamatanluar'>
                                </div>
                                <label class='col-sm-1'>Kelurahan</label>
                                <div class='col-md-4'>
                                    <input type='text' class='form-control' name='t_kelurahanluar' id='t_kelurahanluar'>
                                </div>
                            </div>";
                    }
                }
                $data = $this->Tools()->getService('SkpdTable')->getByKecamatan($data_get->s_idkec);
                $opsi = "";
                $opsi .= "<option value=''>Silahkan Pilih</option>";
                foreach ($data as $r) {
                    $opsi .= "<option value='" . $r['s_idkel'] . "'>" . str_pad($r['s_kodekel'], 3, "0", STR_PAD_LEFT) . " || " . $r['s_namakel'] . "</option>";
                }
//                $res->setContent($opsi);
            
        }
        $data_render = array(
            'res' => $opsi,
            'keckelluar' => $s,
            'keckelluar_badan' => $s
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }


    public function hapusAction() {
        $this->Tools()->getService('SkpdTable')->hapusData($this->params('page'));
        return $this->getResponse();
    }

    public function cetakdaftarskpdAction() {
        $dataskpd = $this->Tools()->getService('SkpdTable')->getdaftarskpd();
        $ar_pemda = $this->Tools()->getService('PemdaTable')->getdata();
        $pdf = new \LosPdf\View\Model\PdfModel();
        $pdf->setVariables(array(
            'dataskpd' => $dataskpd,
            'ar_pemda' => $ar_pemda
        ));
        $pdf->setOption("paperSize", "potrait");
        return $pdf;
    }
}
