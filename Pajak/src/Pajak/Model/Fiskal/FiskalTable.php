<?php

namespace Pajak\Model\Fiskal;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Debug\Debug;

class FiskalTable extends AbstractTableGateway
{

    protected $table = 't_fiskal';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new FiskalBase());
        $this->initialize();
    }

    public function getFiskalId($t_idfiskal)
    {
        $rowset = $this->select(array('t_idfiskal' => $t_idfiskal));
        $row = $rowset->current();
        return $row;
    }

    public function getTransaksilast($idobjek)
    {
        $sql = "SELECT * FROM t_transaksi where t_idwpobjek = '" . $idobjek . "' ORDER BY t_tglpendataan DESC LIMIT 1";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();

        return $res;
    }

    public function simpanfiskal(FiskalBase $base, $session, $post)
    {
        $datatransaksiobjek = $this->getTransaksilast($post['t_idwpwrobjek']);
        $t_tgldaftar = $post['t_tgldaftar'];
        $no = $this->nourutfiskal();
        if (empty($base->t_idfiskal)) {
            // $t_tgldaftar = date('Y-m-d', strtotime($base->t_tgldaftar));
            $t_nourut = (int) $no['t_nomorurut'] + 1;
            $tgl_exp = mktime(0, 0, 0, date('m', strtotime($t_tgldaftar)) + 6, date('d', strtotime($t_tgldaftar)) - 1, date('Y', strtotime($t_tgldaftar)));
        } else {
            if ($post['t_tgldaftar_sebelumnya'] == $base->t_tgldaftar) {
                if ($post['t_tgljatuhtempo_sebelumnya'] != $base->t_tgljatuhtempo) {
                    // $t_tgldaftar = date('Y-m-d', strtotime($base->t_tgldaftar));
                    $t_nourut = (int) $base->t_nomorurut;
                    $tgl_exp = strtotime($base->t_tgljatuhtempo);
                } else {
                    // $t_tgldaftar = date('Y-m-d', strtotime($base->t_tgldaftar));
                    $t_nourut = (int) $base->t_nomorurut;
                    $tgl_exp = strtotime($base->t_tgljatuhtempo);
                }
            } else {
                if ($post['t_tgljatuhtempo_sebelumnya'] != $base->t_tgljatuhtempo) {
                    // $t_tgldaftar = date('Y-m-d', strtotime($base->t_tgldaftar));
                    $t_nourut = (int) $base->t_nomorurut;
                    $tgl_exp = strtotime($base->t_tgljatuhtempo);
                } else {
                    // $t_tgldaftar = date('Y-m-d', strtotime($base->t_tgldaftar));
                    $t_nourut = (int) $base->t_nomorurut;
                    $tgl_exp = mktime(0, 0, 0, date('m', strtotime($base->t_tgldaftar)) + 6, date('d', strtotime($base->t_tgldaftar)) - 1, date('Y', strtotime($base->t_tgldaftar)));
                }
            }
        }
        $data = array(
            't_idwpwr'    => $base->t_idwpwr,
            't_idwpwrobjek'    => $base->t_idwpwrobjek,
            't_nomorurut' => $t_nourut,
            't_tgldaftar' => date('Y-m-d', strtotime($t_tgldaftar)),
            't_jnsusaha'  => $base->t_jnsusaha,
            // 't_jnsfiskal'  => $base->t_jnsfiskal,
            't_pemilik' => $base->t_pemilik,
            't_alamat_tinggal' => $base->t_alamat_tinggal,
            't_ket_keperluan' => $base->t_ket_keperluan,
            't_tgljatuhtempo' => date('Y-m-d', $tgl_exp)
        );
        // var_dump($data);
        // exit;
        if (empty($base->t_idfiskal)) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_idfiskal' => $base->t_idfiskal));
        }
        return $data;
    }

    public function nourutfiskal()
    {
        $sql = "select max(t_nomorurut) as t_nomorurut from t_fiskal where extract(year from t_tgldaftar) = '" . date('Y') . "' ";
        // $sql = "select max(t_nomorurut) as t_nomorurut from t_fiskal";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    //    public function temukanPembayaran($t_idwp) {
    //        $sql = new Sql($this->adapter);
    //        $select = $sql->select();
    //        $select->from(array(
    //            "a" => "t_wp"
    //        ));
    //        $where = new Where();
    //        $where->equalTo('a.t_idwp', $t_idwp);
    //        $select->where($where);
    //        $select->order("a.t_tgldaftar DESC");
    //        $state = $sql->prepareStatementForSqlObject($select);
    //        $res = $state->execute();
    //        return $res;
    //    }

    public function getcomboIdJenisusaha()
    {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_tipeusaha');
        $select->order('s_idusaha asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $selectData = array();
        foreach ($res as $row) {
            $selectData[$row['s_idusaha']] = str_pad($row['s_idusaha'], 2, "0", STR_PAD_LEFT) . " || " . $row['s_namausaha'];
        }
        return $selectData;
    }

    public function getDataWP($t_npwpd)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_wp'
        ));
        $select->join(array(
            "b" => "view_wp"
        ), "a.t_idwp = b.t_idwp", array(
            "t_npwpd", "t_alamat_lengkap"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_npwpd', $t_npwpd);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataFiskalWP($t_idfiskal)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_fiskal'
        ));
        $select->join(array(
            "b" => "view_wp"
        ), "a.t_idwpwr = b.t_idwp", array(
            "t_npwpd", "t_nama", "t_alamat_lengkap", "t_alamat"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "s_tipeusaha"
        ), "a.t_jnsusaha = c.s_idusaha", array(
            "s_namausaha"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_wpobjek"
        ), "a.t_idwpwrobjek = d.t_idobjek", array(
            "t_nop", "t_namaobjek", "t_alamatlengkapobjek"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "e" => "t_wp"
        ), "a.t_idwpwr = e.t_idwp", array(
            "t_nama"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idfiskal', $t_idfiskal);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        // echo $select->getSqlstring();
        // exit();
        $res = $state->execute()->current();
        return $res;
    }

    public function getGridCount(FiskalBase $base, $session, $post)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_fiskal"
        ));
        $select->join(array(
            "b" => "view_wp"
        ), "a.t_idwpwr = b.t_idwp", array(
            "t_npwpd", "t_nama", "t_alamat_lengkap", "t_alamat"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "s_jenisusahafiskal"
        ), "a.t_jnsusaha = c.s_idjenis", array(
            "s_namajenisusaha"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_wpobjek"
        ), "a.t_idwpwrobjek = d.t_idobjek", array(
            "t_nop", "t_namaobjek", "t_alamatlengkapobjek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        if ($post->t_nomorurut != '')
            $where->literal("a.t_nomorurut::text LIKE '%$post->t_nomorurut%'");
        if ($post->t_tgldaftar != '') {
            $t_tgl = explode(' - ', $post->t_tgldaftar);
            $where->literal("a.t_tgldaftar between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("b.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_namausaha != '')
            $where->literal("d.t_namaobjek ILIKE '%$post->t_namausaha%'");
        if ($post->t_namapemilik != '')
            $where->literal("b.t_pemilik ILIKE '%$post->t_namapemilik%'");
        if ($post->t_alamat != '')
            $where->literal("b.t_alamat_lengkap ILIKE '%$post->t_alamat%'");
        if ($post->t_tgljatuhtempo != '') {
            $t_tgl = explode(' - ', $post->t_tgljatuhtempo);
            $where->literal("a.t_tgljatuhtempo between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        //        $where->equalTo('a.t_idwp', (int) $session);
        // if ($base->kolomcari != 'undefined') {
        //     if ($base->combocari != "undefined") {
        //         if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
        //             $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
        //         } elseif ($base->combooperator == "carisama") {
        //             $where->equalTo($base->combocari, $base->kolomcari);
        //         }
        //     }
        // }
        $select->where($where);
        $select->order('t_nomorurut desc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(FiskalBase $base, $offset, $session, $post)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_fiskal"
        ));
        $select->join(array(
            "b" => "view_wp"
        ), "a.t_idwpwr = b.t_idwp", array(
            "t_npwpd", "t_nama", "t_alamat_lengkap", "t_alamat"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "s_tipeusaha"
        ), "a.t_jnsusaha = c.s_idusaha", array(
            "s_namausaha"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_wpobjek"
        ), "a.t_idwpwrobjek = d.t_idobjek", array(
            "t_nop", "t_namaobjek", "t_alamatlengkapobjek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        if ($post->t_nomorurut != '')
            $where->literal("a.t_nomorurut::text LIKE '%$post->t_nomorurut%'");
        if ($post->t_tgldaftar != '') {
            $t_tgl = explode(' - ', $post->t_tgldaftar);
            $where->literal("a.t_tgldaftar between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        if ($post->t_npwpd != '')
            $where->literal("b.t_npwpd ILIKE '%$post->t_npwpd%'");
        if ($post->t_namausaha != '')
            $where->literal("d.t_namaobjek ILIKE '%$post->t_namausaha%'");
        if ($post->t_namapemilik != '')
            $where->literal("b.t_pemilik ILIKE '%$post->t_namapemilik%'");
        if ($post->t_alamat != '')
            $where->literal("b.t_alamat_lengkap ILIKE '%$post->t_alamat%'");
        if ($post->t_tgljatuhtempo != '') {
            $t_tgl = explode(' - ', $post->t_tgljatuhtempo);
            $where->literal("a.t_tgljatuhtempo between '" . date('Y-m-d', strtotime($t_tgl[0])) . "' and '" . date('Y-m-d', strtotime($t_tgl[1])) . "'");
        }
        //        $where->equalTo('a.t_idwp', (int) $session);
        // if ($base->kolomcari != 'undefined') {
        //     if ($base->combocari != "undefined") {
        //         if ($base->combooperator == "carilike" || $base->combooperator == 'undefined') {
        //             $where->literal("$base->combocari LIKE '%$base->kolomcari%'");
        //         } elseif ($base->combooperator == "carisama") {
        //             $where->equalTo($base->combocari, $base->kolomcari);
        //         }
        //     }
        // }
        $select->where($where);
        // if ($base->sortasc != 'undefined') {
        //     if ($base->combosorting != "undefined") {
        //         $select->order("$base->combosorting $base->sortasc");
        //     } else {
        //         $select->order("t_nomorurut desc");
        //     }
        // } elseif ($base->sortdesc != 'undefined') {
        //     if ($base->combosorting != "undefined") {
        //         $select->order("$base->combosorting $base->sortdesc");
        //     } else {
        //         $select->order("t_nomorurut desc");
        //     }
        // } else {
        $select->order("t_tgldaftar desc");
        $select->order("t_nomorurut desc");
        // }
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataWpId($t_idwp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_wp'
        ));
        $select->join(array(
            "b" => "view_wp"
        ), "a.t_idwp = b.t_idwp", array(
            "t_npwpd", "t_alamat_lengkap"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idwp', (int)$t_idwp);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataObjekByIdWp($t_idwp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wpobjek'
        ));
        $where = new Where();
        $where->equalTo('t_idwp', (int)$t_idwp);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataWpObjek($t_wpobjek)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 'view_wpobjek'
        ));
        $where = new Where();
        $where->equalTo('t_idobjek', (int)$t_wpobjek);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataDaftarFiskal($tgldaftar0, $tgldaftar1, $jnsfiskal)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => 't_fiskal'
        ));
        $select->join(array(
            "b" => "view_wp"
        ), "a.t_idwpwr = b.t_idwp", array(
            "t_npwpd", "t_nama", "t_alamat_lengkap", "t_alamat"
        ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "view_wpobjek"
        ), "a.t_idwpwrobjek = d.t_idobjek", array(
            "t_nop", "t_namaobjek", "t_alamatlengkapobjek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        if (!empty($jnsfiskal)) {
            $where->equalTo("a.t_jnsfiskal", $jnsfiskal);
        }
        $where->between("a.t_tgldaftar", date('Y-m-d', strtotime($tgldaftar0)), date('Y-m-d', strtotime($tgldaftar1)));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
}
