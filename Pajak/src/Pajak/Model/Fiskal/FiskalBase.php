<?php

namespace Pajak\Model\Fiskal;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class FiskalBase implements InputFilterAwareInterface {

    public $t_idfiskal, $t_tgldaftar, $t_nomorurut, $t_idwpwr, $t_idwpwrobjek, $t_alamat_tinggal, $t_pemilik, $t_jnsusaha, $t_ket_keperluan, $t_tgljatuhtempo, $t_status, $t_jnsfiskal;
    
    public $page, $direction, $combocari, $kolomcari, $combosorting, $sortasc, $sortdesc, $combooperator;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->t_idfiskal= (isset($data['t_idfiskal'])) ? $data['t_idfiskal'] : null;
        $this->t_tgldaftar = (isset($data['t_tgldaftar'])) ? $data['t_tgldaftar'] : null;
        $this->t_nomorurut = (isset($data['t_nomorurut'])) ? $data['t_nomorurut'] : null;
        $this->t_idwpwr = (isset($data['t_idwpwr'])) ? $data['t_idwpwr'] : null;
        $this->t_idwpwrobjek = (isset($data['t_idwpwrobjek'])) ? $data['t_idwpwrobjek'] : null;
        $this->t_pemilik = (isset($data['t_pemilik'])) ? $data['t_pemilik'] : null;
        $this->t_alamat_tinggal= (isset($data['t_alamat_tinggal'])) ? $data['t_alamat_tinggal'] : null;
        $this->t_jnsusaha = (isset($data['t_jnsusaha'])) ? $data['t_jnsusaha'] : null;
        $this->t_jnsfiskal = (isset($data['t_jnsfiskal'])) ? $data['t_jnsfiskal'] : null;
        $this->t_ket_keperluan = (isset($data['t_ket_keperluan'])) ? $data['t_ket_keperluan'] : null;
        $this->t_tgljatuhtempo = (isset($data['t_tgljatuhtempo'])) ? $data['t_tgljatuhtempo'] : null;
        $this->t_status = (isset($data['t_status'])) ? $data['t_status'] : null;

        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->direction = (isset($data['direction'])) ? $data['direction'] : null;
        $this->combocari = (isset($data['combocari'])) ? $data['combocari'] : null;
        $this->kolomcari = (isset($data['kolomcari'])) ? $data['kolomcari'] : null;
        $this->combosorting = (isset($data['combosorting'])) ? $data['combosorting'] : null;
        $this->sortasc = (isset($data['sortasc'])) ? $data['sortasc'] : null;
        $this->sortdesc = (isset($data['sortdesc'])) ? $data['sortdesc'] : null;
        $this->combooperator = (isset($data['combooperator'])) ? $data['combooperator'] : null;

        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
