<?php

namespace Pajak\Model\Pemeriksaan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PemeriksaanskpdtTable extends AbstractTableGateway {

    protected $table = 't_skpdt';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PemeriksaanskpdtBase());
        $this->initialize();
    }

    public function getjenissurat($s_idsurat) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_jenissurat");
        $where = new Where();
        $where->equalTo("s_idsurat", $s_idsurat);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function simpanskpdt(PemeriksaanskpdtBase $base, $session) {
        // Kode Provinsi dan Kab/Kota
        $kdProvkabkota = '01';
        $no = $this->maxNoSKPDT();
        $t_noskpdt = (int) $no['t_noskpdt'] + 1;

        // Jatuh Tempo SKPDTWS
        $t_tgljatuhtempofix = date('Y-m-d', strtotime("+30 days" . $base->t_tglpemeriksaan));
        
        if(!empty($base->t_dasarrevisi)){
            $t_dasarrevisi = str_ireplace('.', '', $base->t_dasarrevisi);
        }else{
            $t_dasarrevisi = 0 ;
        }
        
        if(!empty($base->t_selisihdasar)){
            $t_selisihdasar = str_ireplace('.', '', $base->t_selisihdasar);
        }else{
            $t_selisihdasar = 0 ;
        }
        
        // Kode Bayar Self => 5-SKPDT
        $jenissurat = $this->getjenissurat(10);
        $data = array(
            't_idtransaksi' => $base->t_idtransaksi,
            't_nopemeriksaan' => $base->t_nopemeriksaan,
            't_noskpdt' => $t_noskpdt,
            't_periodeskpdt' => $base->t_periodepemeriksaan,
            't_tglskpdt' => date('Y-m-d', strtotime($base->t_tglpemeriksaan)),
            't_tgljatuhtemposkpdt' => $t_tgljatuhtempofix,
            't_tarifpajak' => $base->t_tarifpajak,
            't_dasarrevisi' => $t_dasarrevisi,
            't_selisihdasar' => $t_selisihdasar,
            't_jmlhbln' => $base->t_jmlhbln,
            't_tarifpajak' => 0,
            't_tarifpersen' => $base->t_tarifpersen,
            't_jmlhdendaskpdt' => str_ireplace('.', '', $base->t_jmlhdendapemeriksaan),
            't_jmlhpajakseharusnya' => str_ireplace('.', '', $base->t_jmlhpajakseharusnya),
            't_tarifkenaikan' => $base->t_tarifkenaikan,
            't_jmlhkenaikan' => str_ireplace('.', '', $base->t_jmlhkenaikan),
            't_jmlhpajakskpdt' => str_ireplace('.', '', $base->t_jmlhpajakpemeriksaan),
            't_kodebayarskpdt' => $kdProvkabkota.''.str_pad($base->t_jenispajak, 2, "0", STR_PAD_LEFT).''.str_pad($jenissurat['s_idsurat'], 2, "0", STR_PAD_LEFT) . "" . substr($base->t_periodepemeriksaan,2) . "" . str_pad($t_noskpdt, 5, "0", STR_PAD_LEFT),
            't_totalskpdt' => str_ireplace('.', '', $base->t_totalpemeriksaan),
            't_operatorskpdt' => $session['s_iduser']
        );
        $table_skpdt = new \Zend\Db\TableGateway\TableGateway('t_skpdt', $this->adapter);
        $table_skpdt->insert($data);
        return $data;
    }

    public function simpanskpdkb(PemeriksaanskpdtBase $base, $session) {
        // Kode Provinsi dan Kab/Kota
    //    var_dump($base);exit;
        $kdProvkabkota = $this->getprefix4digitkodebayar()['t_prefixkodebayar'];
        
        $no = $this->maxNoSKPDKB();
        $t_noskpdkb = (int) $no['t_noskpdkb'] + 1;

        // Jatuh Tempo SKPDKB
        $t_tgljatuhtempofix = date('Y-m-d', strtotime("+30 days" . $base->t_tglpemeriksaan));
        
        $t_nopemeriksaan = (!empty($base->t_nopemeriksaan)) ? $base->t_nopemeriksaan : '-';


        $jenis = (int) $base->t_jenispajak ;

        if ( $jenis == 4){
            $t_tarifpajak = 25;
            $t_dasarrevisi = str_ireplace('.', '', $base->t_jmlhpajakseharusnya) / $t_tarifpajak * 100;
            $t_selisihdasar = str_ireplace('.', '', $base->t_jmlhpajakpemeriksaan) / $t_tarifpajak * 100;
        }else if ( $jenis == 8 ){
            $t_tarifpajak = 20;
            $t_dasarrevisi = str_ireplace('.', '', $base->t_jmlhpajakseharusnya) / $t_tarifpajak * 100;
            $t_selisihdasar = str_ireplace('.', '', $base->t_jmlhpajakpemeriksaan) / $t_tarifpajak * 100 ;
        }else{
            $base->t_tarifpajak = 0;
            $t_dasarrevisi = 0;
            $t_selisihdasar = 0;
        }
       
        // var_dump($jenis);exit;

        // Kode Bayar Self => 5-SKPDKB
        $jenissurat = $this->getjenissurat(5);
        $data = array(
            't_idtransaksi' => $base->t_idtransaksi,
            't_nopemeriksaan' => $t_nopemeriksaan,
            't_noskpdkb' => $t_noskpdkb,
            't_periodeskpdkb' => $base->t_periodepemeriksaan,
            't_tglskpdkb' => date('Y-m-d', strtotime($base->t_tglpemeriksaan)),
            't_tgljatuhtemposkpdkb' => $t_tgljatuhtempofix,
            't_tarifpajak' => $t_tarifpajak,
            't_dasarrevisi' =>  $t_dasarrevisi,
            't_selisihdasar' =>  $t_selisihdasar,
            't_jmlhbln' => $base->t_jmlhbln,
            't_tarifpersen' => $base->t_tarifpersen,
            't_jmlhdendaskpdkb' => str_ireplace('.', '', $base->t_jmlhdendapemeriksaan),
            't_jmlhpajakseharusnya' => str_ireplace('.', '', $base->t_jmlhpajakseharusnya),
            't_jmlhpajakskpdkb' => str_ireplace('.', '', $base->t_jmlhpajakpemeriksaan),
            't_kodebayarskpdkb' => $kdProvkabkota.'1'.str_pad($base->t_jenispajak, 2, "0", STR_PAD_LEFT).''.str_pad($jenissurat['s_idsurat'], 2, "0", STR_PAD_LEFT) . "" . substr($base->t_periodepemeriksaan,2) . "" . str_pad($t_noskpdkb, 5, "0", STR_PAD_LEFT),
            't_totalskpdkb' => str_ireplace('.', '', $base->t_totalpemeriksaan),
            't_operatorskpdkb' => $session['s_iduser'],
            't_jenispemeriksaan' => $base->t_jenispemeriksaan
        );
        // var_dump($data);exit;
        $table_skpdkb = new \Zend\Db\TableGateway\TableGateway('t_skpdkb', $this->adapter);
        $table_skpdkb->insert($data);
        return $data;
    }

    public function hapusPemeriksaan($id, $session) {
        $data = array(
            'is_deluserpembayaran' => $session['s_iduser']
        );
        $this->update($data, array('t_idtransaksi' => $id));
    }


    public function maxNoSKPDT() {
        $sql = "select max(t_noskpdt) as t_noskpdt from t_skpdt";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function maxNoSKPDKB() {
        $sql = "select max(t_noskpdkb) as t_noskpdkb from t_skpdkb";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function maxNoSKPDKBT() {
        $sql = "select max(t_noskpdkbt) as t_noskpdkbt from t_skpdkbt";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function maxNoSKPDN() {
        $sql = "select max(t_noskpdn) as t_noskpdn from t_skpdn";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function maxNoSKPDLB() {
        $sql = "select max(t_noskpdlb) as t_noskpdlb from t_skpdlb";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getprefix4digitkodebayar()
    {   
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("prefix_kode_bayar");
        $select->columns(array(
            "t_prefixkodebayar"
        ));
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();   
        return $res;
    }

}
