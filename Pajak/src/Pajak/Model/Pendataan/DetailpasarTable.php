<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailpasarTable extends AbstractTableGateway {

    protected $table = 't_detailpasar', $table_tarif = 's_tarifpasar', $table_klasifikasi = 't_klasifikasi_pasar', $view = 'view_harga_pasar';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function simpandetailpasarOld($post, $dataparent) {
        $data = array(
            't_idtransaksi' => $dataparent['t_idtransaksi'],
            't_idkorek' => $post['t_klasifikasi'],
            't_jenisbangunan' => $post['t_jenisbangunan'],
            't_keteranganpasar' => $post['t_keteranganpasar'],
            't_nokios' => $post['t_nokios'],
            't_panjang' => (!empty($post['t_panjang'])) ? $post['t_panjang'] : 0,
            't_lebar' => (!empty($post['t_lebar'])) ? $post['t_lebar'] : 0,
            't_luas' => (!empty($post['t_luas'])) ? $post['t_luas'] : 0,
            't_tarifdasar' => str_ireplace(".", "", $post['t_tarifdasar']),
            't_jmlhbln' => $post['t_jmlhbln']
            // 't_potongan' => str_ireplace(".", "", $post['t_potongan'])
        );
        $t_idtransaksi = $post['t_idtransaksi'];
        if (empty($t_idtransaksi)) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_idtransaksi' => $t_idtransaksi));
        }
        return $data;
    }

    public function simpandetailpasar($datapost, $dataparent) {
        // $this->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));
        for ($i = 0; $i < count($datapost['t_idtarif']); $i++) {
            if (!empty($datapost['t_idtarif'][$i])) {
                $data = array(
                    // 't_idpasar' => '',
                    't_idtransaksi' => $dataparent['t_idtransaksi'],
                    't_idkorek' => $datapost['t_idkorek'],
                    't_volume' => str_ireplace(",", ".", $datapost['t_volume'][$i]),
                    't_hargadasar' => str_ireplace(".", "", $datapost['t_hargadasar'][$i]),
                    't_satuan' => str_ireplace(".", "", $datapost['t_satuan'][$i]),
                    't_idtarif' => $datapost['t_idtarif'][$i],
                    't_pajak' => str_ireplace(".", "", $datapost['t_jumlah'][$i]),
                    // 't_pajak' => str_ireplace(".", "", $datapost['t_pajak'][$i]),
                );
                // var_dump($data);exit();
                $this->insert($data);
            }
        }
    }


    public function simpandetailkebersihan($post, $dataparent) {
        $data = array(
            't_idtransaksi' => $dataparent['t_idtransaksi'],
            't_idklasifikasi' => $post['t_klasifikasi'],
            't_idtarif' => $post['t_kategori'],
            't_tarifdasar' => str_ireplace(".", "", $post['t_tarifdasar']),
            't_jmlhbln' => $post['t_jmlhbln']
            // 't_potongan' => str_ireplace(".", "", $post['t_potongan'])
        );
        $t_idkebersihan = $post['t_idkebersihan'];
        if (empty($t_idkebersihan)) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_idkebersihan' => $t_idkebersihan));
        }
        return $data;
    }

    public function getPendataanPasarByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi"
        ));
        $select->join(array(
            "b" => "t_detailpasar"
                ), "a.t_idtransaksi = b.t_idtransaksi", 
                array("*"), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "view_rekening"
                ), "a.t_idkorek = c.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        // $select->join(array(
        //     "d" => "t_klasifikasi_pasar"
        //         ), "d.t_idklasifikasi = b.t_idklasifikasi", 
        //         array("t_namaklasifikasi"=>"t_keterangan"), $select::JOIN_LEFT);
        $select->join(array(
            "e" => "s_tarifpasar"
                ), "e.s_idtarif= b.t_idtarif", 
                array("s_tarifdasar"=>"s_tarifdasar"), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('b.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        // echo $select->getSqlstring(); exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getDataDetailPasar($t_idtransaksi) {
        
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->view);
        $where = new Where();
        $where->equalTo('t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        // echo $select->getSqlstring(); exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getcomboIdKlasifikasi() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table_klasifikasi);
        $select->order('t_idklasifikasi asc');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $selectData = array();
        foreach ($res as $row) {
            $selectData[$row['t_idklasifikasi']] = $row['t_idklasifikasi'] . " || " . $row['t_keterangan']." ";
        }
        return $selectData;
    }

    public function getDataKlasifikasiKategori($klasifikasi) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table_tarif);
        $where = new Where();
        $where->equalTo('s_idklasifikasi', $klasifikasi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataKlasifikasiTarif($s_idtarif) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table_tarif);
        $where = new Where();
        $where->equalTo('s_idtarif', $s_idtarif);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPendataanPelayananKesehatanByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailretribusi"
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getDetailPasarByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_detailpasar"
        ));
        $select->join(array(
            "b" => "s_rekening"
        ), "a.t_idtarif = b.s_idkorek", 
        array("s_namakorek"), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        // echo $select->getSqlstring(); exit();
        $res = $state->execute();
        return $res;
    }
}
