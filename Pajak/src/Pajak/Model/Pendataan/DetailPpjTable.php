<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DetailPpjTable extends AbstractTableGateway {

    protected $table = 't_detailppj';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DetailPpjBase());
        $this->initialize();
    }

    public function simpanpendataanppj($datapost, $dataparent) {      

        $this->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));
         
         
        for ($i = 0; $i < count($datapost['t_idppj']); $i++) {
            if (!empty($datapost['t_idppj'][$i])) {
                $data = array(
                    // 't_jmlhpajak' => str_ireplace(".", "", $datapost['t_pemakaian'][$i]),

                    't_idtransaksi' => $dataparent['t_idtransaksi'],
                    't_idkorek' => $datapost['t_idkorek'],
                    't_idppj' => $datapost['t_idppj'][$i],
                    't_batas_daya' => $datapost['t_batas_daya'][$i],
                    't_tarif_dasar' => $datapost['t_tarif_dasar'][$i],
                    't_pemakaian' => str_ireplace(".", "", $datapost['t_pemakaian'][$i]),
                    't_tarif_pajak' => $datapost['t_tarif_pajak'][$i],
                    't_pajak' => $datapost['t_pajak'][$i],
                    't_dasarpengenaan' => $datapost['t_dasarpengenaan'][$i],
                    't_golongan' => $datapost['t_golongan'][$i],
                    't_keterangan' =>  $datapost['t_keterangan'][$i],
                );
                $this->insert($data);
            }
        }
    }

    public function simpanpendataanppjold(DetailPpjBase $base, $dataparent) {

        $data = array(
            't_idtransaksi' => $dataparent['t_idtransaksi'],
            't_nilailistrik' => str_ireplace('.', '', $base->t_nilailistrik),
            't_idkorek' => $base->t_idkorek,
            't_subtotalpajak' => str_ireplace('.', '', $base->t_subtotalpajak),
            't_pajak' => str_ireplace('.', '', $base->t_pajak),
        );
        $t_iddetailppj = $base->t_iddetailppj;
        if ($t_iddetailppj == NULL) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_iddetailppj' => $t_iddetailppj));
        }
        return $data;
    }
    
    public function getDetailByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getPendataanByIdTransaksi($t_idtransaksi) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.t_idkorek = b.s_idkorek", array(
            "s_idkorek", "korek", "s_namakorek", "s_persentarifkorek", "s_tarifdasarkorek"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "s_ppj"
                ), "c.t_idppj = a.t_idppj", array(
            "t_idppj", "t_klasifikasi"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "d" => "s_ppj_klasifikasi"
                ), "d.t_idklasifikasi = c.t_klasifikasi", array(
            "t_klasifikasi"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        $select->order(['s_tipekorek' => 'asc', 's_kelompokkorek' => 'asc', 's_jeniskorek' => 'asc', 's_objekkorek' => 'asc', 's_rinciankorek' => 'asc', 's_sub1korek' => 'asc']);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

}
