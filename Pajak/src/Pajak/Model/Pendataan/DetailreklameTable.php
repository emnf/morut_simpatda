<?php

namespace Pajak\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Debug\Debug;

class DetailreklameTable extends AbstractTableGateway
{

    protected $table = 't_detailreklame';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DetailreklameBase());
        $this->initialize();
    }

    public function hapusdetailreklame($t_idtransaksi)
    {
        $this->delete(array('t_idtransaksi' => $t_idtransaksi));
    }

    public function simpanpendataanreklame($datapost, $dataparent)
    {

        $this->delete(array('t_idtransaksi' => $dataparent['t_idtransaksi']));

        // for ($i = 0; $i < count($datapost['t_jenisreklame']); $i++) {
        //     if (!empty($datapost['t_jenisreklame'][$i])) {

        $data = array(
            't_idtransaksi' => $dataparent['t_idtransaksi'],
            't_idkorek' => $datapost['t_idkorek'],
            't_jenisreklame' => $datapost['t_jenisreklame'],
            't_naskah' => $datapost['t_naskah'],
            't_lokasi' => $datapost['t_lokasi'],
            't_tarifreklame' => $datapost['t_tarifpajak'],
            't_panjang' => $datapost['t_panjang'],
            't_lebar' => $datapost['t_lebar'],
            't_jumlah' => $datapost['t_jumlah'],
            't_jangkawaktu' => $datapost['t_jangkawaktu'],
            't_tipewaktu' => $datapost['t_tipewaktu'],
            't_sudutpandang' => $datapost['t_sudutpandang'],
            't_kawasan' => $datapost['t_kawasan'],
            't_nsr' => str_ireplace(".", "", $datapost['t_nsr']),
            't_jmlhpajaksub' => str_ireplace(".", "", $datapost['t_jmlhpajak']),
            't_jenisrokok' => str_ireplace(".", "", $datapost['t_jenisrokok']),
            's_kawasan' => $datapost['s_kawasan'],
            's_sudutpandang' => $datapost['s_sudutpandang'],
            's_kelasjalan' => $datapost['s_kelasjalan'],
            's_ketinggian' => $datapost['s_ketinggian'],
            't_jenisrokok' => $datapost['t_jenisrokok'],
        );

        $this->insert($data);
        // }
        // }
        return $data;
    }

    public function getDetailReklameByIdTransaksi($t_idtransaksi)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        // $select->join(array(
        //     "b" => "view_rekening"
        // ), "a.t_idkorek = b.s_idkorek", array(
        //     "s_rinciankorek"
        // ), $select::JOIN_LEFT);
        // $select->join(array(
        //     "c" => "s_tarif_reklame"
        // ), "a.t_jenisreklame = c.s_idtarif", array(
        //     "s_kawasan", "s_pl", "s_termin"
        // ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('a.t_idtransaksi', (int) $t_idtransaksi);
        $select->where($where);
        // echo $select->getSqlString();
        // exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
}
