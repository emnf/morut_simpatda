<?php

namespace Pajak\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class SkpdTable extends AbstractTableGateway {

    protected $table = 't_kantorskpd';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SkpdBase());
        $this->initialize();
    }

    public function getdata() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function savedata(SkpdBase $kb, $session) {
        $data = array(
            't_namaskpd' => $kb->t_namaskpd,
            't_jalanskpd' => $kb->t_jalanskpd,
            't_idkecskpd' => $kb->t_idkecskpd,
            't_kecamatanskpd' => $kb->t_kecamatanskpd,
            't_idkelskpd' => $kb->t_idkelskpd,
            't_kelurahanskpd' => $kb->t_kelurahanskpd,
            'is_userpendaftaran' => 0,
            't_tutupskpd' =>0,
            'is_tutupskpd' => 0
        );
        // var_dump($data);exit;
        $id = (int) $kb->t_idskpd;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_idskpd' => $kb->t_idskpd));
        }
    }

    

    public function getSkpdId($s_idskpd) {
        /*$rowset = $this->select(array('s_idskpd' => $s_idskpd));
        $row = $rowset->current();
        return $row;*/
        
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_kantorskpd");
        $where = new \Zend\Db\Sql\Where();
        $where->literal('t_idskpd = '.$s_idskpd.'');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getdaftarskpd() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_kantorskpd");
        $select->join("s_kecamatan", "s_kecamatan.s_idkec = t_kantorskpd.t_idkecskpd", ["s_kodekec", "s_namakec"], "left");
        $select->join("s_kelurahan", "s_kelurahan.s_idkel = t_kantorskpd.t_idkelskpd", ["s_kodekel", "s_namakel"], "left");
        $where = new \Zend\Db\Sql\Where();
        //$where->literal('s_idskpd = '.$s_idskpd.'');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function hapusData($id) {
        $this->delete(array('t_idskpd' => $id));
    }
    
    //========================================== datagrid skpd
    public function getjumlahdata($select) {
        
        $sql = new Sql($this->adapter);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridCountSkpd(SkpdBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('t_kantorskpd');
        $select->join("s_kecamatan", "s_kecamatan.s_idkec = t_kantorskpd.t_idkecskpd", ["s_kodekec", "s_namakec"], "left");
        $select->join("s_kelurahan", "s_kelurahan.s_idkel = t_kantorskpd.t_idkelskpd", ["s_kodekel", "s_namakel"], "left");
        $where = new Where();
        if ($base->t_namaskpd != '')
            $where->literal("t_namaskpd LIKE '%$base->t_namaskpd%'");
        if ($base->t_jalanskpd != '')
            $where->literal("t_jalanskpd LIKE '%$base->t_jalanskpd%'");
        if ($base->t_kecamatanskpd != '')
            $where->literal("t_kecamatanskpd LIKE '%$base->t_kecamatanskpd%'");
        if ($base->t_kelurahanskpd!= '')
            $where->literal("t_kelurahanskpdLIKE '%$base->t_kelurahanskpd%'");
  
        // $where->isNotNull('');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataSkpd(SkpdBase $base, $start) { 
        
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('t_kantorskpd');
        $select->join("s_kecamatan", "s_kecamatan.s_idkec = t_kantorskpd.t_idkecskpd", ["s_kodekec", "s_namakec"], "left");
        $select->join("s_kelurahan", "s_kelurahan.s_idkel = t_kantorskpd.t_idkelskpd", ["s_kodekel", "s_namakel"], "left");
        $where = new Where();
        if ($base->t_namaskpd != '')
            $where->literal("t_namaskpd LIKE '%$base->t_namaskpd%'");
        if ($base->t_jalanskpd != '')
            $where->literal("t_jalanskpd LIKE '%$base->t_jalanskpd%'");
        if ($base->t_kecamatanskpd != '')
            $where->literal("s_namakec LIKE '%$base->t_kecamatanskpd%'");
        if ($base->t_kelurahanskpd != '')
            $where->literal("s_namakel LIKE '%$base->t_kelurahanskpd%'");
      // $where->isNotNull('');
        $select->where($where);
        $select->order('t_idskpd asc');
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        //////////////////////////////////////////////
        // echo '<pre>';
        // print_r(\Zend\Stdlib\ArrayUtils::iteratorToArray($res));
        // echo '</pre>';
        // exit();
        //////////////////////////////////////////////
         
        return $res;
    }
    
    
   
 
    //========================================== end datagrid skpd
    
    public function getByKecamatan($id) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_kelurahan');
        $where = new Where();
        $where->literal("s_idkeckel = ".$id."");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function cekid_kecamatan($id) {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_kecamatan');
        $where = new Where();
        $where->literal("s_idkec = ".$id."");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        
        return $res;
    }
    
    public function getcomboIdKecamatan() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_kecamatan');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        $selectData = array();
        foreach ($res as $row) {
            $selectData[$row['s_idkec']] = str_pad($row['s_kodekec'], 2, "0", STR_PAD_LEFT) . " || " . $row['s_namakec'];
        }
        return $selectData;
    }
    
    
   

}
