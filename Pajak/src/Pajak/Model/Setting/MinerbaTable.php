<?php

namespace Pajak\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class MinerbaTable extends AbstractTableGateway
{
    protected $table = 's_tarif_minerba';
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new MinerbaBase());
        $this->initialize();
    }

    public function getGridCount(MinerbaBase $base, $post)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $select->join(array(
            "b" => "view_rekening"
        ), "b.s_idkorek = a.s_idkorek", array(
            "korek", "s_namakorek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        if ($post->t_jenisminerba != '')
        $where->literal("a.s_namajenis::text LIKE '%$post->t_jenisminerba%'");
    if ($post->t_namarekening != '')
        $where->literal("b.s_namakorek::text LIKE '%$post->t_namarekening%'");
    
    if ($post->t_tarif != '')
        $where->literal("a.s_tarif::text LIKE '$$post->t_tarif%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(MinerbaBase $base, $offset, $post)
    {
        // var_dump($post);exit;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $select->join(array(
            "b" => "view_rekening"
        ), "b.s_idkorek = a.s_idkorek", array(
            "korek", "s_namakorek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        if ($post->t_jenisminerba != '')
            $where->literal("a.s_namajenis::text LIKE '%$post->t_jenisminerba%'");
        if ($post->t_namarekening != '')
            $where->literal("b.s_namakorek::text LIKE '%$post->t_namarekening%'");
        
        if ($post->t_tarif != '')
            $where->literal("a.s_tarif::text LIKE '$$post->t_tarif%'");

        $select->where($where);
        $select->order("a.s_idkorek asc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function savedata(MinerbaBase $base, $session){
         $data = array(
            's_idkorek' => $base->s_idkorek,
            's_namajenis' => $base->s_namajenis,
            's_tarif' => str_ireplace(".", "", $base->s_tarif)
        );
        $id = (int) $base->s_idtarif;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_idtarif' => $id));
        }
    }

    public function getDataId($id)
    {
        $rowset = $this->select(array('s_idtarif' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($id)
    {
        $this->delete(array('s_idtarif' => $id));
    }

    public function getDataIdKorek($id)
    {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        $where->equalTo('s_idkorek', (int) $id);
        $select->where($where);
        $select->order("s_namajenis asc");
        $res = $sql->prepareStatementForSqlObject($select)->execute();
        return $res;
    }

    public function getDataTarifMinerba($t_jenisminerba)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => $this->table
        ));
        $select->join(array(
            "b" => "s_rekening"
        ), "b.s_idkorek = a.s_idkorek", array(
            "s_persentarifkorek"
        ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('s_idtarif', (int) $t_jenisminerba);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
}