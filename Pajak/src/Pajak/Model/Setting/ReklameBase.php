<?php

namespace Pajak\Model\Setting;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ReklameBase implements InputFilterAwareInterface
{

    public $s_idtarif, $s_idkorek, $s_namajenis, $s_termin, $s_tarif, $s_panjang, $s_lebar, $s_kawasan;

    public $page, $direction, $combocari, $kolomcari, $combosorting, $sortasc, $sortdesc, $combooperator;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->s_idtarif = (isset($data['s_idtarif'])) ? $data['s_idtarif'] : null;
        $this->s_idkorek = (isset($data['s_idkorek'])) ? $data['s_idkorek'] : null;
        $this->s_namajenis = (isset($data['s_namajenis'])) ? $data['s_namajenis'] : null;
        $this->s_termin = (isset($data['s_termin'])) ? $data['s_termin'] : null;
        $this->s_panjang = (isset($data['s_panjang'])) ? $data['s_panjang'] : null;
        $this->s_lebar = (isset($data['s_lebar'])) ? $data['s_lebar'] : null;
        $this->s_kawasan = (isset($data['s_kawasan'])) ? $data['s_kawasan'] : null;
        $this->s_tarif = (isset($data['s_tarif'])) ? $data['s_tarif'] : null;

        $this->combocari = (isset($data['combocari'])) ? $data['combocari'] : null;
        $this->kolomcari = (isset($data['kolomcari'])) ? $data['kolomcari'] : null;
        $this->combosorting = (isset($data['combosorting'])) ? $data['combosorting'] : null;
        $this->sortasc = (isset($data['sortasc'])) ? $data['sortasc'] : null;
        $this->sortdesc = (isset($data['sortdesc'])) ? $data['sortdesc'] : null;
        $this->combooperator = (isset($data['combooperator'])) ? $data['combooperator'] : null;
        $this->direction = (isset($data['direction'])) ? $data['direction'] : null;
        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}
