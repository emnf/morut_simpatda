<?php

namespace Pajak\Model\Setting;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SkpdBase implements InputFilterAwareInterface {

    public $t_idskpd, $t_namaskpd, $t_jalanskpd, $t_idkecskpd, $t_kecamatanskpd, $t_idkelskpd, $t_kelurahanskpd;
    public $page, $direction, $combocari, $kolomcari, $combosorting, $sortasc, $sortdesc, $combooperator;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->t_idskpd = (isset($data['t_idskpd'])) ? $data['t_idskpd'] : null;
        $this->t_namaskpd = (isset($data['t_namaskpd'])) ? $data['t_namaskpd'] : null;
        $this->t_jalanskpd = (isset($data['t_jalanskpd'])) ? $data['t_jalanskpd'] : null;
        $this->t_idkecskpd = (isset($data['t_idkecskpd'])) ? $data['t_idkecskpd'] : null;
        $this->t_kecamatanskpd = (isset($data['t_kecamatanskpd'])) ? $data['t_kecamatanskpd'] : null;
        $this->t_idkelskpd = (isset($data['t_idkelskpd'])) ? $data['t_idkelskpd'] : null;
        $this->t_kelurahanskpd = (isset($data['t_kelurahanskpd'])) ? $data['t_kelurahanskpd'] : null;

        $this->combocari = (isset($data['combocari'])) ? $data['combocari'] : null;
        $this->kolomcari = (isset($data['kolomcari'])) ? $data['kolomcari'] : null;
        $this->combosorting = (isset($data['combosorting'])) ? $data['combosorting'] : null;
        $this->sortasc = (isset($data['sortasc'])) ? $data['sortasc'] : null;
        $this->sortdesc = (isset($data['sortdesc'])) ? $data['sortdesc'] : null;
        $this->combooperator = (isset($data['combooperator'])) ? $data['combooperator'] : null;
        $this->direction = (isset($data['direction'])) ? $data['direction'] : null;
        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 't_namaskpd',
                        'required' => true
            )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 't_jalanskpd',
                        'required' => false
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 't_idkecskpd',
                        'required' => true
            )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 't_idkelskpd',
                        'required' => true
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
