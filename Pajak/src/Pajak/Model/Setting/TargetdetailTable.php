<?php

namespace Pajak\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class TargetdetailTable extends AbstractTableGateway {

    protected $table = 's_targetdetail';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new TargetdetailBase());
        $this->initialize();
    }

    public function getDataId($id) {
        $rowset = $this->select(array('s_idtarget' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getdata() {
        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function simpantargetdetail(TargetdetailBase $kc, $session) {
        $data = array(
            's_idtargetheader' => $kc->s_idtargetheader,
            's_targetrekening' => $kc->s_targetrekening,
            's_targetjumlah' => str_ireplace(".", "", $kc->s_targetjumlah)
        );
        $id = (int) $kc->s_idtargetdetail;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_idtargetdetail' => $kc->s_idtargetdetail));
        }
    }

    public function temukanTargetdetail($id_header) {
        $tglakhir = '2021';
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        // $select->from(array(
            // 'a' => 's_targetdetail'
        // ));
        // $select->join(array(
            // "b" => "view_rekening"
                // ), "a.s_targetrekening = b.s_idkorek", array(
            // "korek", "s_namakorek"
                // ), $select::JOIN_LEFT);
		
		$select->from("s_rekening");
        $select->columns(array(
            "s_idkorek",
            "s_objekkorek",
            "s_tipekorek",
            "s_kelompokkorek",
            "s_jeniskorek",
            "s_objekkorek",
            "s_rinciankorek",
            "s_sub1korek",
            "s_sub2korek",
            "s_sub3korek",
            "korek" => new \Zend\Db\Sql\Expression("(CASE WHEN " .
                    "s_kelompokkorek = '0' THEN s_tipekorek " .
                    "WHEN s_jeniskorek = '0' or s_jeniskorek = '00' THEN s_tipekorek || '.' || s_kelompokkorek " .
                    "WHEN s_objekkorek = '0' or s_objekkorek = '00' THEN s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek " .
                    "WHEN s_rinciankorek = '00' THEN s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek  " .
                    "WHEN s_sub1korek IS NULL OR s_sub1korek = '' THEN " .
                    "s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek || '.' || s_rinciankorek " .
                    "WHEN s_sub2korek IS NULL OR s_sub2korek = '' THEN " .
                    "s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek || '.' || s_rinciankorek || '.' || s_sub1korek " .
                    "WHEN s_sub3korek IS NULL OR s_sub3korek = '' THEN " .
                    "s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek || '.' || s_rinciankorek || '.' || s_sub1korek || s_sub2korek " .
                    "ELSE " .
                    "s_tipekorek || '.' || s_kelompokkorek || '.' || s_jeniskorek || '.' || s_objekkorek || '.' || s_rinciankorek || '.' || s_sub1korek || '.' || s_sub2korek || '.' || s_sub3korek " .
                    "END )"),
            "s_namakorek",
            "s_targetjumlah" => new \Zend\Db\Sql\Expression("(case when s_rekening.s_kelompokkorek != '0' THEN " .
                    "case when s_rekening.s_jeniskorek != '00' THEN " .
                    "case when s_rekening.s_objekkorek != '00' THEN " .
                    "case when s_rekening.s_rinciankorek != '00' THEN " .
                    "case when s_rekening.s_sub1korek != '0' or s_rekening.s_sub1korek != '' THEN " .
                    "case when s_rekening.s_sub2korek != '0' or s_rekening.s_sub2korek != '' THEN " .
                    "case when s_rekening.s_sub3korek != '0' or s_rekening.s_sub3korek != '' THEN " .
                    "(SELECT coalesce(sum(s_targetjumlah), 0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "LEFT JOIN s_rekening za on za.s_idkorek=s_targetdetail.s_targetrekening " .
                    "WHERE s_target.s_idtarget = " . $id_header . " " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    "AND za.s_sub3korek = s_rekening.s_sub3korek " .
                    ") " .
                    "ELSE " .
                    "(SELECT coalesce(sum(s_targetjumlah), 0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "LEFT JOIN s_rekening za on za.s_idkorek=s_targetdetail.s_targetrekening " .
                    "WHERE s_target.s_idtarget = " . $id_header . " " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    "AND za.s_sub2korek = s_rekening.s_sub2korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(s_targetjumlah), 0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "LEFT JOIN s_rekening za on za.s_idkorek=s_targetdetail.s_targetrekening " .
                    "WHERE s_target.s_idtarget = " . $id_header . " " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    "AND za.s_sub1korek = s_rekening.s_sub1korek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(s_targetjumlah), 0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "LEFT JOIN s_rekening za on za.s_idkorek=s_targetdetail.s_targetrekening " .
                    "WHERE s_target.s_idtarget = " . $id_header . " " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    "AND za.s_rinciankorek = s_rekening.s_rinciankorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(s_targetjumlah), 0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "LEFT JOIN s_rekening za on za.s_idkorek=s_targetdetail.s_targetrekening " .
                    "WHERE s_target.s_idtarget = " . $id_header . " " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    "AND za.s_objekkorek = s_rekening.s_objekkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(s_targetjumlah), 0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "LEFT JOIN s_rekening za on za.s_idkorek=s_targetdetail.s_targetrekening " .
                    "WHERE s_target.s_idtarget = " . $id_header . " " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    "AND za.s_jeniskorek = s_rekening.s_jeniskorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(s_targetjumlah), 0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "LEFT JOIN s_rekening za on za.s_idkorek=s_targetdetail.s_targetrekening " .
                    "WHERE s_target.s_idtarget = " . $id_header . " " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    "AND za.s_kelompokkorek = s_rekening.s_kelompokkorek " .
                    ") " .
                    "END " .
                    "ELSE " .
                    "(SELECT coalesce(sum(s_targetjumlah), 0) FROM s_target " .
                    "LEFT JOIN s_targetdetail on s_target.s_idtarget = s_targetdetail.s_idtargetheader " .
                    "LEFT JOIN s_rekening za on za.s_idkorek=s_targetdetail.s_targetrekening " .
                    "WHERE s_target.s_idtarget = " . $id_header . " " .
                    "AND za.s_tipekorek = s_rekening.s_tipekorek " .
                    ") " .
                    "END )"),
             "s_idtargetdetail" => new \Zend\Db\Sql\Expression("(select s_idtargetdetail from s_target
                    left join s_targetdetail on s_targetdetail.s_idtargetheader = s_target.s_idtarget
                        where s_targetdetail.s_targetrekening = s_rekening.s_idkorek
                        and s_target.s_idtarget = ".$id_header.")
                 ")
            ));
        $where = new Where();
//        $where->equalTo('s_idtargetheader', $id_header);
        $where->literal("extract(year from s_rekening.s_tglawalkorek) <= '" . date('Y', strtotime($tglakhir)) . "'");
        $where->literal("extract(year from s_rekening.s_tglakhirkorek) >= '" . date('Y', strtotime($tglakhir)) . "'");
        $select->where($where);
        $select->order(new \Zend\Db\Sql\Expression("s_rekening.s_tipekorek, " .
                "s_rekening.s_kelompokkorek, " . "s_rekening.s_jeniskorek, " . "s_rekening.s_objekkorek, " .
                "s_rekening.s_rinciankorek, " . "s_rekening.s_sub1korek DESC"));
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function temukanTargetdetailRekening($id_header, $rekening = NULL) {
        if ($rekening != NULL):
            $whererek = " AND rek.s_idkorek=$rekening ";
        endif;
        $sql = "SELECT rek.s_idkorek,rek.korek, rek.s_namakorek, 
(
SELECT SUM(t0.s_targetjumlah) FROM s_targetdetail t0
LEFT JOIN view_rekening t_rek ON t_rek.s_idkorek=t0.s_targetrekening
WHERE t_rek.s_tipekorek=rek.s_tipekorek
AND t_rek.s_kelompokkorek=rek.s_kelompokkorek
AND t_rek.s_jeniskorek=rek.s_jeniskorek
AND t_rek.s_objekkorek=rek.s_objekkorek
AND t0.s_idtargetheader=$id_header
) as jumlahanggaran
FROM view_rekening rek 
WHERE rek.s_rinciankorek='00' $whererek
order by korek";
        $st = $this->adapter->query($sql);
        $res = $st->execute();
        return $res->current();
    }

    public function temukanTargetdetailById($id_detail) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            'a' => 's_targetdetail'
        ));
        $select->join(array(
            "b" => "view_rekening"
                ), "a.s_targetrekening = b.s_idkorek", array(
            "korek", "s_namakorek"
                ), $select::JOIN_LEFT);
        $where = new Where();
        $where->equalTo('s_idtargetdetail', $id_detail);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function hapusData($id) {
        $this->delete(array('s_idtargetdetail' => $id));
    }

}
