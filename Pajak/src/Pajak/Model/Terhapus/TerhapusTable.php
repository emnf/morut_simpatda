<?php

namespace Pajak\Model\Terhapus;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;

class TerhapusTable extends AbstractTableGateway {

    protected $table = 't_transaksi_cancel';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new TerhapusBase());
        $this->initialize();
    }

    public function getTerhapusId($t_idtransaksi) {
        $rowset = $this->select(array('t_idtransaksi' => $t_idtransaksi));
        $row = $rowset->current();
        return $row;
    }


    
    public function getGridCount(TerhapusBase $base, $post) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi_cancel"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwpobjek = b.t_idobjek", array(
            "*"
                ), $select::JOIN_LEFT);
        $select->join(array(
            "c" => "s_users"
                ), "a.t_userhapus = c.s_iduser", array(
            "*"
                ), $select::JOIN_LEFT);
       
        $where = new Where();
      
     
        if ($post->t_npwpd != '')
            $where->literal("t_npwpdwp ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("t_namawp ILIKE '%$post->t_nama%'");
        if ($post->t_namaobjek != '')
            $where->literal("t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_nopenetapan != '')
            $where->literal("a.t_nourut::text ::textLIKE '%$post->t_nourut%'");
        if ($post->t_jmlhpajak != '')
            $where->literal("a.t_jmlhpajak::text LIKE '%$post->t_jmlhpajak%'");
       
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(TerhapusBase $base, $offset, $post) {
        //  var_dump($post);exit;
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi_cancel"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwpobjek = b.t_idobjek", array(
            "*"
                ), $select::JOIN_LEFT);
       $select->join(array(
            "c" => "s_users"
                ), "a.t_userhapus = c.s_iduser", array(
            "*"
                ), $select::JOIN_LEFT);
       
        $where = new Where();
       
   
        if ($post->t_npwpd != '')
            $where->literal("t_npwpdwp ILIKE '%$post->t_npwpd%'");
        if ($post->t_nama != '')
            $where->literal("t_namawp ILIKE '%$post->t_nama%'");
        if ($post->t_namaobjek != '')
            $where->literal("t_namaobjek ILIKE '%$post->t_namaobjek%'");
        if ($post->t_nopenetapan != '')
            $where->literal("a.t_nourut::text  LIKE '%$post->t_nourut%'");
        if ($post->t_jmlhpajak != '')
            $where->literal("a.t_jmlhpajak::text LIKE '%$post->t_jmlhpajak%'");
       
        $select->where($where);
        $select->order("a.t_tglpendataan desc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    

    public function getdataterhapus($tglpendataan0,$tglpendataan1) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array(
            "a" => "t_transaksi_cancel"
        ));
        $select->join(array(
            "b" => "view_wpobjek"
                ), "a.t_idwpobjek = b.t_idobjek", array(
            "*"
                ), $select::JOIN_LEFT);
      
        $where = new Where();
        $where->literal("t_tglpendataan between '".date('Y-m-d',strtotime($tglpendataan0))."' and  '".date('Y-m-d',strtotime($tglpendataan1))."' ");
        $select->where($where);
        $select->order("a.t_tglpendataan desc");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
}
