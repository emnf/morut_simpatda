<?php

namespace Pajak\Form\Fiskal;

use Zend\Form\Form;

class FiskalFrm extends Form
{

    public function __construct($nourut = null, $comboid_jenisusaha = null)
    {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idfiskal',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idfiskal',
            )
        ));

        $this->add(array(
            'name' => 't_idwpwr',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idwpwr',
            )
        ));

        $this->add(array(
            'name' => 't_idwpwrobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idwpwrobjek',
            )
        ));

        $this->add(array(
            'name' => 't_tgldaftar_sebelumnya',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tgldaftar_sebelumnya',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'value' => date('d-m-Y'),
                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 't_tgljatuhtempo_sebelumnya',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tgljatuhtempo_sebelumnya',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'value' => date('d-m-Y'),
                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 't_tgldaftar',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tgldaftar',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'value' => date('d-m-Y'),
                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 't_tgljatuhtempo',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tgljatuhtempo',
                'class' => 'bootstrap-datepicker form-control',
                'required' => true,
                'value' => date('d-m-Y'),
                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 't_nomorurut',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nomorurut',
                'class' => 'form-control',
                'required' => true,
                'value' => $nourut,
                'readonly' => true,
            )
        ));


        $this->add(array(
            'name' => 't_npwpd',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_npwpd',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
            )
        ));


        $this->add(array(
            'name' => 't_nama',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nama',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 't_alamat',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_alamat',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
            )
        ));


        $this->add(array(
            'name' => 't_alamat_tinggal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_alamat_tinggal',
                'class' => 'form-control',
                'required' => true,
                //                'readonly' => true,
            )
        ));


        $this->add(array(
            'name' => 't_pemilik',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_pemilik',
                'class' => 'form-control',
                'required' => true,
                //                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 't_jnsusaha',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jnsusaha',
                'class' => 'form-control',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_jenisusaha,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_jnsfiskal',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_jnsfiskal',
                'class' => 'form-control',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => [
                    1 => '01 || Fiskal Tender',
                    2 => '02 || Fiskal SIUP'
                ],
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_ket_keperluan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_ket_keperluan',
                'class' => 'form-control', //ckeditor
                'required' => true,
            )
        ));

        $this->add(array(
            'name' => 'Fiskalsubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'Fiskalsubmit',
                'class' => "btn btn-warning btn-block"
            )
        ));
    }
}
