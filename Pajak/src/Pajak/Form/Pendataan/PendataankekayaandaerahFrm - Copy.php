<?php

namespace Pajak\Form\Pendataan;

use Zend\Form\Form;
use Zend\Form\Element\Select;

class PendataankekayaandaerahFrm extends Form {

    public function __construct($comboKekayaan = null) {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idtransaksi',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idtransaksi',
            )
        ));

        $this->add(array(
            'name' => 't_idkekayaan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkekayaan',
            )
        ));

        $this->add(array(
            'name' => 't_idobjek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idobjek',
            )
        ));

        $this->add(array(
            'name' => 't_idkorek',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idkorek',
            )
        ));

        $this->add(array(
            'name' => 't_jenispajak',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_jenispajak',
            )
        ));
        
        $this->add(array(
            'name' => 't_operatorpendataan',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_operatorpendataan',
            )
        ));

        $this->add(array(
            'name' => 't_nourut',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nourut',
                'class' => 'form-control',
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_periodepajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_periodepajak',
                'class' => 'form-control',
                'value' => date('Y'),
                'onchange' => 'CariPendataanByObjek();',
                'onblur' => 'CariPendataanByObjek();'
            )
        ));

        $this->add(array(
            'name' => 't_tglpenetapan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tglpenetapan',
                'class' => 'bootstrap-datepicker form-control',
                'value' => date('d-m-Y'),
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaawal',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaawal',
                'class' => 'bootstrap-datepicker form-control',
                'value' => date('d-m-Y'),
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_masaakhir',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_masaakhir',
                'class' => 'bootstrap-datepicker form-control',
                'value' => date('d-m-Y'),
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 't_korek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_korek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_namakorek',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namakorek',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true
            )
        ));

        $this->add(array(
            'name' => 't_klasifikasi',
            'type' => Select::class,
            'attributes' => array(
                'id' => 't_klasifikasi',
                'class' => 'form-control',
                'onchange' => 'carikekayaandaerah()',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboKekayaan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_kategorisatu',
            'type' => Select::class,
            'attributes' => array(
                'id' => 't_kategorisatu',
                'class' => 'form-control',
                'onchange' => 'carikategorikekayaan()',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboKekayaan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_kategoridua',
            'type' => Select::class,
            'attributes' => array(
                'id' => 't_kategoridua',
                'class' => 'form-control',
                'onchange' => 'caritarifkekayaan()',
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
//                'value_options' => $comboKekayaan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));
        
        $this->add(array(
            'name' => 't_jmlhbln',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhbln',
                'class' => 'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'onkeyup' => 'hitungretribusikekayaan()',
                'onblur' => 'hitungretribusikekayaan()'
            )
        ));

        $this->add(array(
            'name' => 't_satuan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_satuan',
                'class' => 'form-control',
                'required' => true,
                'readonly' => true,
            )
        ));
        
        $this->add(array(
            'name' => 't_tarifretribusi',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_tarifretribusi',
                'class' => 'form-control',
                'readonly' => true,
            )
        ));
        
        $this->add(array(
            'name' => 't_luastanah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luastanah',
                'class' => 'form-control',
//                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungretribusikekayaan()',
                'onblur' => 'hitungretribusikekayaan()'
            )
        ));

        $this->add(array(
            'name' => 't_luasbangunan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_luasbangunan',
                'class' => 'form-control',
//                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungretribusikekayaan()',
                'onblur' => 'hitungretribusikekayaan()'
            )
        ));

        $this->add(array(
            'name' => 't_hargatanah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargatanah',
                'class' => 'form-control',
//                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungretribusikekayaan();this.value = formatCurrency(this.value);',
                'onblur' => 'hitungretribusikekayaan();this.value = formatCurrency(this.value);',
                'onkeyup' => 'hitungretribusikekayaan();this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));

        $this->add(array(
            'name' => 't_nilailuastanah',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nilailuastanah',
                'class' => 'form-control',
//                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));
        
        $this->add(array(
            'name' => 't_nilailuasbangunan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_nilailuasbangunan',
                'class' => 'form-control',
//                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right'
            )
        ));

        $this->add(array(
            'name' => 't_hargadasarsewa',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_hargadasarsewa',
                'class' => 'form-control',
                'readonly' => true,
            )
        ));
        
        $this->add(array(
            'name' => 't_potongan',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_potongan',
                'class' => 'form-control',
//                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'hitungretribusirumahdinas();this.value = formatCurrency(this.value);',
                'onblur' => 'hitungretribusirumahdinas();this.value = formatCurrency(this.value);',
                'onkeyup' => 'hitungretribusirumahdinas();this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
                'value' => 0
            )
        ));

        $this->add(array(
            'name' => 't_jmlhpajak',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jmlhpajak',
                'class' => 'form-control',
//                'required' => true,
                'readonly' => true,
                'style' => 'text-align:right; background:#000099; color: white; padding: 7px 10px; height:40px; font-size: 16px; font-weight:bolder'
            )
        ));
        
        $this->add(array(
            'name' => 'Pendataansubmit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Simpan',
                'id' => 'Pendataansubmit',
                'class' => "btn btn-warning btn-block"
            )
        ));
    }
}
