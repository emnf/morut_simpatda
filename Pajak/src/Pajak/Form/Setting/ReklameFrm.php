<?php

namespace Pajak\Form\Setting;

use Zend\Form\Form;

class ReklameFrm extends Form
{

    public function __construct($cmb_rekening = null)
    {
        parent::__construct();

        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 's_idtarif',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 's_idtarif'
            )
        ));

        $this->add(array(
            'name' => 's_namajenis',
            'type' => 'text',
            'attributes' => array(
                'id' => 's_namajenis',
                'class' => 'form-control',
                'required' => true
            )
        ));

        $this->add(array(
            'name' => 's_idkorek',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 's_idkorek',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => 'Silahkan pilih',
                'value_options' => $cmb_rekening,
            )
        ));

        $this->add(array(
            'name' => 's_termin',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 's_termin',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => 'Silahkan pilih',
                'value_options' => [
                    'Terbit' => 'Terbit',
                    'Hari' => 'Hari',
                    'Minggu' => 'Minggu',
                    'Bulan' => 'Bulan',
                    'Tahun' => 'Tahun',
                ],
            )
        ));

        $this->add(array(
            'name' => 's_panjang',
            'type' => 'text',
            'attributes' => array(
                'id' => 's_panjang',
                'class' => 'form-control',
                'onKeyPress' => "return numbersonly(this, event);",
                'value' => 0


            )
        ));

        $this->add(array(
            'name' => 's_lebar',
            'type' => 'text',
            'attributes' => array(
                'id' => 's_lebar',
                'class' => 'form-control',
                'onKeyPress' => "return numbersonly(this, event);",
                'value' => 0



            )
        ));

        $this->add(array(
            'name' => 's_kawasan',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 's_kawasan',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => 'Silahkan pilih',
                'value_options' => [
                    'Kawasan Strategis I' => 'Kawasan Strategis I',
                    'Kawasan Strategis II' => 'Kawasan Strategis II',
                    'Kawasan Strategis III' => 'Kawasan Strategis III',
                ],
            )
        ));

        $this->add(array(
            'name' => 's_tarif',
            'type' => 'text',
            'attributes' => array(
                'id' => 's_tarif',
                'class' => 'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'this.value = formatCurrency(this.value);',
                'onblur' => 'this.value = formatCurrency(this.value);',
                'onkeyup' => 'this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));

        $this->add(array(
            'type' => 'submit',
            'name' => 'simpan',
            'attributes' => array(
                'value' => 'Simpan',
                'class' => 'btn btn-primary btn-sm',
            ),
        ));
    }
}
