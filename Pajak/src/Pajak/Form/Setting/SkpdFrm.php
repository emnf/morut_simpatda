<?php

namespace Pajak\Form\Setting;

use Zend\Form\Form;

class SkpdFrm extends Form {

    public function __construct($comboid_kecamatan = null, $comboid_kelurahan = null) {

        parent::__construct();
        
        $this->setAttribute("method", "post");

        $this->add(array(
            'name' => 't_idskpd',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 't_idskpd'
            )
        ));

        $this->add(array(
            'name' => 't_namaskpd',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_namaskpd',                
                'class'=>'form-control'
            )
        ));

        $this->add(array(
            'name' => 't_jalanskpd',
            'type' => 'text',
            'attributes' => array(
                'id' => 't_jalanskpd',
                'class' => 'form-control'
            )
        ));

        
        
        $this->add(array(
            'name' => 't_idkecskpd',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_idkecskpd',
                'class' => 'form-control',
                'required' => true,
                'onchange' => 'comboKelurahanCamat();'
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_kecamatan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'name' => 't_idkelskpd',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 't_idkelskpd',
                'class' => 'form-control',
                'required' => true
            ),
            'options' => array(
                'empty_option' => 'Silahkan Pilih',
                'value_options' => $comboid_kelurahan,
                'disable_inarray_validator' => true, // <-- disable
            )
        ));

        $this->add(array(
            'type' => 'submit',
            'name' => 'simpan',
            'attributes' => array(
                'value' => 'Simpan',
                'class' => 'btn btn-primary btn-sm'
            ),
        ));
    }
}