<?php

namespace Pajak\Form\Setting;

use Zend\Form\Form;

class ParkirFrm extends Form {
    
    public function __construct() {
        parent::__construct();
        
        $this->setAttribute("method", "post");
        
        $this->add(array(
            'name' => 's_idtarif',
            'type' => 'hidden',
            'attributes' => array(             
                'id'=>'s_idtarif'
            )
        ));
        
        $this->add(array(
            'name' => 's_keterangan',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_keterangan',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_satuan',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_satuan',
                'class'=>'form-control',
                'required' => true
            )
        ));
        
        $this->add(array(
            'name' => 's_tarif',
            'type' => 'text',
            'attributes' => array(             
                'id'=>'s_tarif',
                'class'=>'form-control',
                'required' => true,
                'style' => 'text-align:right',
                'onchange' => 'this.value = formatCurrency(this.value);',
                'onblur' => 'this.value = formatCurrency(this.value);',
                'onkeyup' => 'this.value = formatCurrency(this.value);',
                'onKeyPress' => "return numbersonly(this, event);",
            )
        ));
        
        $this->add(array(
            'type' => 'submit',
            'name' => 'simpan',
            'attributes' => array(
                'value' => 'Simpan',
                'class' => 'btn btn-primary btn-sm',
            ),
        ));        
    }
    
}